const axios = require("axios")

const checkMsg = async (ctx) => {
    
const  result = await axios({
    method: 'POST',
    // url: 'http://api.weixin.qq.com/wxa/msg_sec_check?access_token=TOKEN',
    url: 'http://api.weixin.qq.com/wxa/msg_sec_check', // 这里就是少了一个token
    data: ({
      openid: ctx.request.headers["x-wx-openid"], // 可以从请求的header中直接获取 req.headers['x-wx-openid']
      version: 2,
      scene: 2,
      content: '安全检测文本'
    })
})
    ctx.body = result

}

